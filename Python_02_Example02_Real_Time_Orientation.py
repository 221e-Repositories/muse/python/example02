from bleak import BleakScanner, BleakClient
import asyncio

import matplotlib.pyplot as plt
import numpy as np
import struct
import matplotlib.pyplot as plt
import math

# Command and Data characteristics
CMD_UUID = "d5913036-2d8a-41ee-85b9-4e361aa5c8a7"
DATA_UUID = "09bf2c52-d1d9-c0b7-4145-475964544307"

# Init orientation quaternions (current and previous)
q_curr = [1, 0, 0, 0]
q_prev = [1, 0, 0, 0]

def quaternion_product(p, q):
    """ Perform product between quaternions.
    Input:
        - quaternion, p
        - quaternion, q
    Output: resultant product quaternion, pd
    """
    # Extract quaternion components
    p0, p1, p2, p3 = p
    q0, q1, q2, q3 = q

    # Perform multiplication
    pq = [
        p0 * q0 - p1 * q1 - p2 * q2 - p3 * q3,
        p0 * q1 + p1 * q0 + p2 * q3 - p3 * q2,
        p0 * q2 - p1 * q3 + p2 * q0 + p3 * q1,
        p0 * q3 + p1 * q2 - p2 * q1 + p3 * q0
    ]

    return pq

def quaternion_conjugate(p):
    """ Perform product between quaternions.
    Input:
        - quaternion, p
    Output: resultant conjugate quaternion, qconj
    """
    qconj = [p[0], -p[1], -p[2], -p[3]]
    return qconj

def quat2_rotation_matrix(q):
    R = np.eye(3)
    R[0, 0] = 1 - 2 * (q[2]**2 + q[3]**2)
    R[1, 1] = 1 - 2 * (q[1]**2 + q[3]**2)
    R[2, 2] = 1 - 2 * (q[1]**2 + q[2]**2)

    R[0, 1] = 2 * (q[1] * q[2] - q[3] * q[0])
    R[0, 2] = 2 * (q[1] * q[3] + q[2] * q[0])
    
    R[1, 0] = 2 * (q[1] * q[2] + q[3] * q[0])
    R[2, 0] = 2 * (q[1] * q[3] - q[2] * q[0])
    
    R[1, 2] = 2 * (q[2] * q[3] - q[1] * q[0])
    R[2, 1] = 2 * (q[2] * q[3] + q[1] * q[0])
    
    return R

def cmd_notification_handler(sender, data):
    return

def data_notification_handler(sender, data):
    """Decode data"""
    global q_curr, q_prev
    global ax, arrow_directions, quiv, i, fig
    
	# Update plot at halved frequency, because 3D plot is slow
    if i % 2 == 0: 
        qi_bytes = data[8:10]
        qj_bytes = data[10:12]
        qk_bytes = data[12:14]

        # Converting bytes to int16
        qi_val = struct.unpack('<h', bytes(qi_bytes))[0]
        qj_val = struct.unpack('<h', bytes(qj_bytes))[0]
        qk_val = struct.unpack('<h', bytes(qk_bytes))[0]

        # Converting int16 to single and normalizing by 32767
        qi_val = qi_val / 32767.0
        qj_val = qj_val / 32767.0
        qk_val = qk_val / 32767.0

        # Compute (unit) quaternion real component
        qw_val = 1.0
        if (1 - (qi_val**2 + qj_val**2 + qk_val**2)) > 0:
            qw_val = math.sqrt(1 - (qi_val**2 + qj_val**2 + qk_val**2))

        # Update orientation quaternion variables
        q_curr = np.array([qw_val, qi_val, qj_val, qk_val])

        # Compute angular difference from quaternions
        qdelta = quaternion_product(q_curr, quaternion_conjugate(q_prev))
        Rquat = quat2_rotation_matrix(qdelta)

        q_prev = q_curr

        # Apply transformation to the model
        arrow_directions = np.transpose(np.matmul(Rquat, arrow_directions.T))

        quiv.remove() #remove previous quiver
        quiv = ax.quiver([0, 0, 0],[0, 0, 0],[0, 0, 0], arrow_directions[0,:], arrow_directions[1, :], arrow_directions[2,:],
                color=['b', 'r', 'y'], arrow_length_ratio=0.1)

		
        fig.canvas.draw()
        plt.pause(0.0001)

    i +=1
    
    return

async def main():
    global ax, arrow_directions, quiv, fig, i
	
    i = 0

    # Device name
    my_device_name = 'muse_v3'

    # Device Enumeration
    devices = await BleakScanner.discover(timeout=10.0)
    myDevice = None
    for d in devices:
        print(d)
        if d.name == my_device_name:
            myDevice = d

    if myDevice != None:
        # Device Connection
        async with BleakClient(str(myDevice.address)) as client:
            print("Device connected.")
            # Set up the figure for real-time plot
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

            # Fix the axes scaling 
            ax.set_box_aspect([1, 1, 1])
            ax.view_init(40,20)

            # Define arrow starting points
            x_start  = [0, 0, 0]
            y_start = [0, 0, 0]
            z_start = [0, 0, 0]

            # Define arrow directions and lengths
            arrow_length = 20
            arrow_directions = np.array([[arrow_length, 0, 0],
                                         [0, arrow_length, 0],
                                         [0, 0, arrow_length]])

            # Plot arrows
            quiv = ax.quiver(x_start, y_start, z_start, arrow_directions[0,:], arrow_directions[1,:], arrow_directions[2,:],
                      color=['b', 'r', 'y'], arrow_length_ratio=0.1)

            # Set axis limits
            ax.set_xlim([-25, 25])
            ax.set_ylim([-25, 25])
            ax.set_zlim([-25, 25])

            # Set axis labels
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')

            fig.canvas.draw()   
            plt.pause(3.0)

            # Check device status
            await client.write_gatt_char(char_specifier=CMD_UUID, data=bytearray([0x82, 0x00]), response=True)
            response = await client.read_gatt_char(CMD_UUID)

            if (response[0] == 0 and response[2] == 0x82 and response[3] == 0x00 and response[4] == 2):
                print('System in IDLE state')

                # Subscribe to Command and Data characteristic
                await client.start_notify(CMD_UUID, cmd_notification_handler)
                await client.start_notify(DATA_UUID, data_notification_handler)

                # Start direct streaming of quaternions data and read data
                print("Start Streaming...")
                await client.write_gatt_char(char_specifier=CMD_UUID, data=bytearray([0x02, 0x05, 0x08, 0x10, 0x00, 0x00, 0x01]), response=True)
                # Make the streaming last for 30 seconds
                await asyncio.sleep(30)
                # Stop the streaming
                await client.write_gatt_char(char_specifier=CMD_UUID, data=bytearray([0x02, 0x01, 0x02]), response=True)
                print("End Streaming....")

			# Unsubscribe from characteristics
            await client.stop_notify(CMD_UUID)
            await client.stop_notify(DATA_UUID)

# Run the main function
asyncio.run(main())
